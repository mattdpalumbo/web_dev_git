// alert('connected');

var p1Button = document.querySelector('#p1');
var p2Button = document.querySelector('#p2');
var resetButton = document.querySelector('#reset');

var p1Display = document.querySelector('#p1Display');
var p2Display = document.querySelector('#p2Display');
var winningScoreDisplay = document.querySelector('#winning')


var numInput = document.querySelector('input');

var p1Score = 0;
var p2Score = 0;
var gameOver = false;

var winningScore = 5;

//player 1
p1Button.addEventListener('click', playerOneActions);

//player 2
p2Button.addEventListener('click', PlayerTwoActions);

resetButton.addEventListener('click', ResetActions);

numInput.addEventListener('change', function(){
	winningScoreDisplay.textContent = numInput.value;	
	winningScore = Number(numInput.value);
	ResetActions();
});

function playerOneActions(){
	if(!gameOver){
		p1Score++;
		if(p1Score === winningScore){
			p1Display.classList.add('winner');
			gameOver = true;
	}
	console.log(p1Score);
	p1Display.textContent = p1Score; 
	}

}

function PlayerTwoActions(){
	if(!gameOver){
		p2Score++;
		if(p2Score === winningScore){
			p2Display.classList.add('winner');
			gameOver = true;
	}
	console.log(p1Score);
	p2Display.textContent = p2Score; 
	}
}

function ResetActions(){
	p1Score = 0;
	p2Score = 0;
	p1Display.textContent = p1Score; 
	p2Display.textContent = p2Score; 
	p1Display.classList.remove('winner');
	p2Display.classList.remove('winner');
	gameOver = false;	
}