//====================================================================================
//             Libraries/ Framework setups
//====================================================================================

var express = require('express');

var app =  express();

//====================================================================================
//              Setting up paths
//====================================================================================


app.get('/', function(req, res){
	res.send('hi there!');
});

app.get('/dog', function(req, res){
	res.send('i love dooggies!');
});

app.get('/bye', function(req, res){
	res.send('goodbye!');
});

//all other routes
app.get('/r/:subredditName', function(req, res){
	var subreddit = req.params.subredditName;
	console.log(req.params);
	res.send('welcome to the ' + subreddit.toUpperCase() + ' subreddit!');
});

app.get('/r/:subbredditName/comments/:id/:title/', function(req, res){
	console.log(req.params);
	res.send('welcomto the commentst');
});


//all other routes - must come after all other routes
app.get('*', function(req, res){
	res.send('you are a star!!!');
});


//====================================================================================
//              Port Setup
//====================================================================================
app.listen(3000, function(){
	console.log('listening on port 3000');
});