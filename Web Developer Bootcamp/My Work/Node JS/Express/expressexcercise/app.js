//====================================================================================
//             Libraries/ Framework setups
//====================================================================================
var express = require('express');
var app = express();


//====================================================================================
//              Setting up paths
//====================================================================================

app.get('/', function(req, res){
	res.send('Hi there welcome to my assignment')
});

app.get('/speak/:animal', function(req, res){
	var animal = req.params.animal;
	if(animal == 'pig')
	{
		res.send('The pig says "oink" ');
	}
	else if (animal == 'cow')
	{
		res.send('The cow says "moo"');
	}
	else if (animal == 'dog')
	{
		res.send('The dog says "woof"');
	}
	else
	{
		res.send('The ' + animal + ' makes a sound! ')
	}
});

app.get('/repeat/:phrase/:num', function(req, res){
	var phrase = req.params.phrase;
	var num = Number(req.params.num);
    res.send((phrase + ' ').repeat(num ));
});

app.get('*', function(req, res){
	res.send('Sorry, page not found... What are you doing with your life????')
});

//====================================================================================
//              Port Setup
//====================================================================================



app.listen(3000, function(){
	console.log('listening on port 3000');
});