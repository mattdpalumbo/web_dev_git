//====================================================================================
//             Libraries/ Framework setups
//====================================================================================
var express = require('express');
var app = express();
var bodyParser = require('body-parser');

app.use(bodyParser.urlencoded({extended: true}));

app.use(express.static('public')); // how to use style sheet directory
app.set('view engine', 'ejs');


//====================================================================================
//              Setting up paths
//====================================================================================

app.get('/', function(req, res){
	res.render('home');
});


var friends = ['sean', 'tucker', 'patty', 'rossi'];
app.post('/addfriend', function(req, res){
	var newFriend = req.body.newfriend;
	friends.push(newFriend);
	res.redirect('/friends');
})


app.get('/friends', function(req, res){
	res.render('friends', {friends: friends});
});






//============= all other paths =====================================================

app.get('*', function(req, res){
	res.send('Sorry, page not found... What are you doing with your life????')
});

//=================================================================================
//              Port Setup
//====================================================================================
app.listen(3000, function(){
	console.log('listening on port 3000');
});