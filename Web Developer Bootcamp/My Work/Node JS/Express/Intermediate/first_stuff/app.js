//====================================================================================
//             Libraries/ Framework setups
//====================================================================================
var express = require('express');
var app = express();

app.use(express.static('public')); // how to use style sheet directory
app.set('view engine', 'ejs');


//====================================================================================
//              Setting up paths
//====================================================================================

app.get('/', function(req, res){
	res.render('home');
});

app.get('/fallinlovewith/:thing', function(req, res){
	var thing = req.params.thing;
	res.render('love', {thingVar: thing});
});

app.get('/posts', function(req, res){
	var posts = [
		{title: "post 1", author: 'susy'},
		{title: "Can you believe it", author: 'matt'},
		{title: 'what a goal', author: 'torrie'},
		{title: "political jargan", author: 'billy'}
	];
	res.render('posts', {posts:posts})
});

//============= all other paths =====================================================

app.get('*', function(req, res){
	res.send('Sorry, page not found... What are you doing with your life????')
});

//=================================================================================
//              Port Setup
//====================================================================================
app.listen(3000, function(){
	console.log('listening on port 3000');
});