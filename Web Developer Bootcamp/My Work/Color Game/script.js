// alert('connected!')

var numOfSquares = 6;
var squares = document.querySelectorAll('.square');
var colors = generateRandomColors(numOfSquares);
var bgColor = '#232323';


var pickedColor = pickColor();
var colorDisplay = document.getElementById('colorDisplay');
var messageDisplay = document.querySelector('#message');
var h1 = document.querySelector('h1');
var body = document.querySelector('body');
var resetButton = document.querySelector('#reset');
var easyBtn = document.querySelector('#easyBtn');
var hardBtn = document.querySelector('#hardBtn');

easyBtn.addEventListener('click', easyButtonAction);

hardBtn.addEventListener('click', hardButtonAction);

gameMechanics();

resetButton.addEventListener('click', reset);

function gameMechanics(){
	colorDisplay.textContent = pickedColor;
	for (i = 0; i < squares.length; i ++){
	//add cololrs to squares 
	squares[i].style.backgroundColor = colors[i];

	//add click listeners to squares
	squares[i].addEventListener('click', function(){
		//grab color of clicked square
		var clickedColor = this.style.backgroundColor;
		//compare color to pickedColor
		if(clickedColor === pickedColor){
			messageDisplay.textContent = 'Correct!'
			changeColors(clickedColor);
			resetButton.textContent = 'Play Again?'
		}
		else {
			this.style.backgroundColor = bgColor;
			messageDisplay.textContent = 'Try Again!'
			}
		});
	}
}

function changeColors(color){
	//loop through all squares
	for(i=0; i < colors.length; i++)
	{
		//change each color to mathc given colors
		squares[i].style.backgroundColor = color;
		h1.style.backgroundColor = color;
	}
}

function pickColor(){
	var random = Math.floor(Math.random() * colors.length);
	return colors[random];
}

function generateRandomColors(num){
	//make an array
	var arr = [];
	//add num rand colors to arr
	for(i = 0; i < num; i++){
		//get random color and push into arr
		arr.push(randomColor());
	}

	//return that array
	return arr;
}

function randomColor(){
	//pick a 'red' from 0 to 255
	var r = Math.floor(Math.random() * 256);
	//pick a 'green' from 0 to 255
	var g = Math.floor(Math.random() * 256);
	//pick a 'blue' from 0 to 255
	var b = Math.floor(Math.random() * 256);
	var color = 'rgb(' + r + ', ' + g + ', ' + b + ')';

	return color;
}

function reset(){
	//generate new colors
	colors = generateRandomColors(numOfSquares);
	//pick a new random color from array
	pickedColor = pickColor();

	//change color display to match picked color
	colorDisplay.textContent = pickedColor;
	//change colors of squares
	gameMechanics();
	messageDisplay.textContent = '';
	body.style.backgroundColor = bgColor;
	h1.style.backgroundColor = 'steelblue';
	this.textContent = 'New Colors';

}

function easyButtonAction(){
	easyBtn.classList.add('selected');
	hardBtn.classList.remove('selected');
	numOfSquares = 3;
	colors = generateRandomColors(numOfSquares);
	pickedColor = pickColor();
	colorDisplay.textContent = pickedColor;
	h1.style.backgroundColor = 'steelblue';
	for(var i = 0; i <squares.length; i++){
		if(colors[i]){
			squares[i].style.backgroundColor = colors[i];
		}
		else{
			squares[i].style.display = 'none';
		}
	}
}

function hardButtonAction(){
	easyBtn.classList.remove('selected');
	hardBtn.classList.add('selected');
	numOfSquares = 6;
	colors = generateRandomColors(numOfSquares);
	pickedColor = pickColor();
	colorDisplay.textContent = pickedColor;
	h1.style.backgroundColor = 'steelblue';
	for(var i = 0; i <squares.length; i++){
		squares[i].style.backgroundColor = colors[i];
		squares[i].style.display = 'block';
	}
}