#challenge 1
SELECT 
email,
created_at
FROM users
WHERE created_at = (SELECT MIN(created_at) FROM users);

#challenge 2
SELECT
DATE_FORMAT(created_at, '%M') AS month,
COUNT(*) AS 'count'
FROM users
GROUP BY DATE_FORMAT(created_at, '%M');

#CHALLENGE 3
SELECT
COUNT(*) AS yahoo_users
FROM users
WHERE email LIKE '%yahoo.com';

#challenge 4
SELECT
CASE 
	WHEN email LIKE '%gmail.com' THEN 'gmail'
	WHEN email LIKE '%yahoo.com' THEN 'yahoo'
	WHEN email LIKE '%hotmail.com' THEN 'hotmail'
	ELSE 'other'
	END AS provider,
COUNT(*)
FROM users
GROUP BY provider;

