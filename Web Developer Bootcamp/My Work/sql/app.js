var faker = require('faker');
var mysql = require('mysql');
var bodyParser = require('body-parser');
var express = require('express');
var app = express();

//configs
app.set('view engine', 'ejs');
app.use(bodyParser.urlencoded({extended: true}));
app.use(express.static(__dirname + "/public"));

//define MySQL connection, password needs to be set after exiting the IDE
//export MYSQL_PW= pwhere
var connection = mysql.createConnection({
	host	: 'localhost',
	user	: 'root',
	password: process.env.MYSQL_PW,
	database: 'tut'
});

// gets setup up url links for ur site like www.mywebsite.com/somethinghere
// use connection.query to access SQL database

//gets from server shows to client
app.get("/", function(req, res){
	
	var q = "SELECT COUNT(*) AS count FROM users";
	connection.query(q, function(err, results){
		if (err) throw err;
		var count = results[0].count;
		res.render('home',{count: count});
	});
});

//request from client to server
app.post('/register', function(req,res){
 var person = {email: req.body.email};
 connection.query('INSERT INTO users SET ?', person, function(err, result) {
 console.log(err);
 console.log(result);
 res.redirect("/");
 });
});

//port your site runs on 
app.listen(3000, function () {
	console.log('app listening on port 3000!');
});



























