var todos = ['Buy new turtle'];
window.setTimeout(function() {
  // put all the rest of your JS code from the lecture here
  var input = prompt('what would you like to do??');

  while(input !== 'quit'){
  	// handle input
  	if (input == 'list'){
  		listToDos();
  	}
  	else if (input == 'new') {
  		addToDo();
  	}
  	else if (input === 'delete'){
  		deleteToDo();
  	}
  	//ask for input again
  	var input = prompt('what would you like to do??');
  }
  console.log('ok, you quit the app');


}, 500);

function listToDos(){
  	todos.forEach(function(todo, i){
  			console.log('**********');
  			console.log(i + ": " + todo);
  			console.log('**********');
  		});
  }

function addToDo(){
	//ask for new todo
  	var newTodo = prompt('Enter new todo');
  	// add to todos array
  	todos.push(newTodo);
  	console.log('added todo');
}

function deleteToDo(){
  	//ask for indext of todo to be deleted
	var index = prompt('enter index of todo to delete');
	//delete that todo
	todos.splice(index,1);
	console.log('deleted todo');
}

function sumArray(array){
	var result = 0;
	array.forEach(function(ele){
		result = result + ele;
	});
	console.log(result);
}

function max(array) {
	var max = 0;
	array.forEach(function(ele){
		if(ele > max){
			max = ele;
		}
	});
	console.log(max);
}