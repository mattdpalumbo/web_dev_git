var mongoose = require('mongoose');

mongoose.connect('mongodb://localhost/cat_app',{ useNewUrlParser: true });

var catSchema = new mongoose.Schema({
	name: String,
	age: Number,
	temperament: String
});

var Cat = mongoose.model('Cat', catSchema);

// //add a cat to the db
// var george = new Cat({
// 	name: "Mrs. Norris",
// 	age: 7,
// 	temperament: "Evil"
// });

// george.save(function(err, cat){
// 	if(err){
// 		console.log('ERROR!')
// 	}
// 	else {
// 		console.log('just saved the cat');
// 		console.log(cat);
// 	}
// })

Cat.create({
	name: 'Poopyboy',
	age: 15,
	temperament: 'Nice'
}, function(err, cat){
	if(err){
		console.log(err);
	}
	else {
		console.log(cat);
	}
});


// retrieve all cats and print them to console
Cat.find({}, function(err, cats){
	if(err){
		console.log('oh no, error!');
		console.log(err);
	}
	else {
		console.log(cats);
	}
})
