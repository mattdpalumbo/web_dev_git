var mongoose = require("mongoose");
var Stadium = require("./models/stadium");
var Comment   = require("./models/comment");
 
var seeds = [
    {
        name: "MUSC Health Stadium", 
        image: "https://i.imgur.com/Z6TXhlx.jpg",
        description: "Home of the battery the mighty mighty battery Home of the battery the mighty mighty battery Home of the battery the mighty mighty battery Home of the battery the mighty mighty battery",
        author: {
                id:'5d4261f25983163af8daee6f',
                username:'MrPoop'
        }
    },
    {
        name: "Old Trafford", 
        image: "https://i.imgur.com/mKYKmaS.jpg",
        description: "Home of Manchester United 'The Theatre of Dreams' Home of Manchester United 'The Theatre of Dreams' Home of Manchester United 'The Theatre of Dreams' Home of Manchester United 'The Theatre of Dreams' ",
        author: {
                id:'5d4261f25983163af8daee6f',
                username:'MrPoop'
        }
    },

    {
        name: "Santiago Bernabeu", 
        image: "https://i.imgur.com/KAwN2cR.jpg",
        description: "Home to Real Madrid, in the heart of Madrid 'Los Blancos, y nada mas' Home to Real Madrid, in the heart of Madrid 'Los Blancos, y nada mas' Home to Real Madrid, in the heart of Madrid 'Los Blancos, y nada mas' ",
        author: {
                id:'5d4261f25983163af8daee6f',
                username:'MrPoop'
        }
    }
];
 
async function seedDB(){
   //Remove all campgrounds
   await Stadium.remove({});
   console.log('Stadiums removed');
    await Comment.remove({});
    console.log('comments removed');
    for(const seed of seeds){
        let stadium = await Stadium.create(seed);
        console.log('stadium created');
         let comment = await Comment.create(
             {
                 text: 'this stadium is flawless',
                 author: {
                    id:'5d4261f25983163af8daee6f',
                    username:'MrPoop'
                }
             });
         console.log('comments created');
         stadium.comments.push(comment);
         stadium.save();
    }
} 
    //add a few comments
 
module.exports = seedDB;