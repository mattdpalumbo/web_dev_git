var express = require('express');
var router = express.Router();
var Stadium = require('../models/stadium');

//INDEX route: show all stadiums
router.get('/', function(req, res){
	console.log(req.user)
	//Get all stadiums from DB
	Stadium.find({}, function(err, allStadiums){
		if(err){
			console.log(err);
		}
		else {
			res.render('stadiums/index', {stadiums: allStadiums});
		}
	})

});

//CREATE route: creates new stadiums 
router.post('/',  isLoggedIn, function(req, res){
	// get data from form and add to stadiums array
	var name = req.body.name;
	var image = req.body.image;
	var description = req.body.description;
	var author = {
		id: req.user.id,
		username: req.user.username
	}
	var newStadium = {name: name, image: image, description: description, author: author}
	//create new stadium and save to DB
	Stadium.create(newStadium, function(err, newlyCreated){
		if(err){
			console.log(err);
		}
		else {
			console.log(newStadium);
			//redirect to stadiums page
			res.redirect('/stadiums');
		}
	});	
});

//NEW - show form to create new stadium
router.get('/new', isLoggedIn, function(req, res){
	res.render('stadiums/new')
});

//SHOW -  shows more info about one stadium
router.get('/:id', function(req,res){
	//find stadium with provided ID
	Stadium.findById(req.params.id).populate("comments").exec( function(err, foundStadium){
		if(err)
		{
			console.log(err);
		}
		else {
			// console.log(foundStadium);
			//render show template with that stadium
			res.render('stadiums/show', {stadium: foundStadium});
		}
	});
});


//EDIT Stadium ROUTE
router.get('/:id/edit', checkStadiumOwnerhsip, function(req, res){
	Stadium.findById(req.params.id, function(err, foundStadium){
		res.render('stadiums/edit', {stadium: foundStadium});
	});
});

//UPDATE Stadium ROUTE
router.put('/:id', checkStadiumOwnerhsip, function(req, res){ 
	//find and update the correct campground
	Stadium.findByIdAndUpdate(req.params.id, req.body.stadium, function(err, updatedStadium){
		if(err){
			res.redirect('/stadiums');
		}
		else {
			res.redirect('/stadiums/'+ req.params.id);
		}
	})
	//redirect to show page
});

//Delete Stadium ROUTE
router.delete('/:id', checkStadiumOwnerhsip, function(req, res){
	Stadium.findByIdAndRemove(req.params.id, function(err){
		if(err){
			res.redirect('/stadiums');
		} else {
			res.redirect('/stadiums');
		}
	})
})


// middleware ================================
function isLoggedIn(req, res, next){
	if(req.isAuthenticated()){
		return next();
	}
	req.flash('error', 'You need to be logged in to do that!');
	res.redirect('/login');
}

function checkStadiumOwnerhsip(req, res, next){
	if(req.isAuthenticated()){
		Stadium.findById(req.params.id, function(err, foundStadium){
			if(err){
				req.flash('error', 'Stadium not found!');
				res.redirect('back');
			} 
			else {
				//does user own stadium?
				if(foundStadium.author.id.equals(req.user.id)){
					next();					
				} else {
					req.flash('error', 'You dont have permission to do that!');
					res.redirect('back');
				}
			}
		});
	} else {
		req.flash('error', 'You need to be logged in to do that!');
		res.redirect('back');
	}
}


module.exports = router;