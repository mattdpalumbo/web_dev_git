var express = require('express');
var router = express.Router({mergeParams:true});
var Stadium = require('../models/stadium');
var Comment = require('../models/comment');


//============= comments routes ======================================================

//comments new
router.get('/new', isLoggedIn, function(req, res){
	Stadium.findById(req.params.id, function(err, stadium){
		if(err)
		{
			console.log(err)
		}
		else{
			res.render('comments/new', {stadium: stadium});
		}
	});
});

//comments create
router.post('/', isLoggedIn, function(req, res){
	//lookup campground using ID
	Stadium.findById(req.params.id, function(err, stadium){
		if(err){
			console.log(err)
			res.redirect("/stadiums");
		} else {
			Comment.create(req.body.comment, function(err, comment){
				if(err){
					console.log(err);
				} else {
					//add username and id to comment
					comment.author.id = req.user.id;
					comment.author.username =  req.user.username;
					//save comment
					comment.save();
					stadium.comments.push(comment);
					stadium.save();
					req.flash('success', 'Sucessfully added comment');
					res.redirect('/stadiums/' + stadium.id);
				}
			});
		}
	});
});

// EDIT COMMENTS
router.get('/:comment_id/edit', checkCommentOwnerhsip, function(req, res){
	Comment.findById(req.params.comment_id, function(err, foundComment){
		if(err){
			res.redirect('back');
		} else {
			res.render('comments/edit', {stadium_id: req.params.id, comment:foundComment});
		}
	});
});

//UPDATE ROUTE
router.put('/:comment_id', checkCommentOwnerhsip, function(req,res){
	Comment.findByIdAndUpdate(req.params.comment_id, req.body.comment, function(err, updatedComment){
		if(err)
		{
			res.redirect('back');
		}
		else{
			res.redirect('/stadiums/'+ req.params.id);
		}
	})
});


//DESTROY ROUTE
router.delete('/:comment_id', checkCommentOwnerhsip, function(req, res){
	Comment.findByIdAndRemove(req.params.comment_id, function(err){
		if(err){
			res.redirect('back');
		} else {
			req.flash('success', 'Comment Deleted');
			res.redirect('/stadiums/'+ req.params.id);
		}
	})
});



//middleware
function isLoggedIn(req, res, next){
	if(req.isAuthenticated()){
		return next();
	}
	req.flash('error', 'Please Login First!');
	res.redirect('/login');
}

function checkCommentOwnerhsip(req, res, next){
	if(req.isAuthenticated()){
		Comment.findById(req.params.comment_id, function(err, foundComment){
			if(err){
				req.flash('error', 'Something went wrong!');
				res.redirect('back');
			} 
			else {
				//does user own comment?
				if(foundComment.author.id.equals(req.user.id)){
					next();					
				} else {
					req.flash('error', 'You dont have permission to do that');
					res.redirect('back');
				}
			}
		});
	} else {
		req.flash('error', 'You need to be logged in to do that!');
		res.redirect('back');
	}
}

module.exports = router;