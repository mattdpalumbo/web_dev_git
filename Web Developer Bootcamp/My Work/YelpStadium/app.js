//====================================================================================
//             Libraries/ Framework setups
//====================================================================================
var express                = require('express'),
    app                    = express(),
    request                = require('request'),
    bodyParser             = require('body-parser'),
    mongoose               = require('mongoose'),
    passport               = require('passport'),
    LocalStrategy          = require('passport-local'),
    passportLocalMongoose  = require('passport-local-mongoose'),
    expressSession         = require('express-session'),
    methodOverride         = require('method-override'),
    Stadium                = require('./models/stadium'),
    seedDB                 = require('./seed'),
    Comment                = require('./models/comment'),
    User                   = require('./models/user'),
    flash                  = require('connect-flash');

//requiring routes
var commentRoutes = require('./routes/comments'),
	stadiumRoutes = require('./routes/stadiums'),
	indexRoutes = require('./routes/index');

// seedDB();
app.use(bodyParser.urlencoded({extended:true}));
app.use(express.static(__dirname + '/public')); // how to use style sheet directory
app.set('view engine', 'ejs');
app.use(methodOverride('_method'));
app.use(flash());
mongoose.connect('mongodb://localhost:27017/yelp_stadium', { useNewUrlParser: true });

//PASS PORT CONFIG

app.use(require('express-session')({
	secret: 'Manchester United is the greatest team in history',
	resave: false,
	saveUninitialized: false
}));

app.use(passport.initialize());
app.use(passport.session());
passport.use(new LocalStrategy(User.authenticate()));
passport.serializeUser(User.serializeUser());
passport.deserializeUser(User.deserializeUser());


app.use(function(req, res, next){
	res.locals.currentUser = req.user;
    res.locals.error = req.flash('error');
    res.locals.success = req.flash('success');
	next();
});
//====================================================================================
//              Routes
//====================================================================================

app.use(indexRoutes);
app.use("/stadiums", stadiumRoutes);
app.use("/stadiums/:id/comments",commentRoutes);

//============= all other paths =====================================================

app.get('*', function(req, res){
	res.send('Sorry, page not found... What are you doing with your life????')
});


//=================================================================================
//              Port Setup
//====================================================================================

app.listen(3000, function(){
	console.log('listening on port 3000');
});